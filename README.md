# ares-cryptography

## About
Programatic Implementation of various Cryptography Algorithms.


## Pre-Requisites

-  Modular Mathematics
-  Python Programming
-  Multiplicative Inverse
-  Python Libraries like ( numpy )
-  Theoretical Understanding of Cryptography & its applications.


## Authors and acknowledgment
Nishant Soni. 


## Project status
- Under Development -