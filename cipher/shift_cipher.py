import string

class ShiftCipher:

    def __init__(self, message,key) -> None:
        self.message = message
        self.key = key

    def encrypt(self,idx_seq):
        encrypted_idx_seq = []
        for i in idx_seq:
            enc = (i + self.key) % self.modulus
            encrypted_idx_seq.append(enc)
        return encrypted_idx_seq

    def decrypt(self,idx_seq):
        decrypted_idx_seq = []
        for i in idx_seq:
            enc = (i + (self.modulus - self.key)) % self.modulus
            decrypted_idx_seq.append(enc)
        return decrypted_idx_seq

    def list_to_string(self,list):
        listToStr = ''.join(map(str,list))
        return listToStr
    
    def encrypt_message(self):
        alpha_list = []
        for alpha in string.ascii_lowercase:
            alpha_list.append(alpha)
        self.modulus = len(alpha_list)
        
        if self.message.isalpha():
            idx_seq = []
            for ltr in self.message:
                idx_in_alpha_list = alpha_list.index((ltr).lower())
                idx_seq.append(idx_in_alpha_list)
            encrypted_idx_seq = self.encrypt(idx_seq)
            encrypted_text = [alpha_list[j] for j in encrypted_idx_seq]
            return(self.list_to_string(encrypted_text))
        else:
            return 'Only text values'
    
    def decrypt_message(self):
        alpha_list = []
        for alpha in string.ascii_uppercase:
            alpha_list.append(alpha)
        self.modulus = len(alpha_list)
        
        if self.message.isalpha():
            idx_seq = []
            for ltr in self.message:
                idx_in_alpha_list = alpha_list.index((ltr).upper())
                idx_seq.append(idx_in_alpha_list)
            decrypted_idx_seq = self.decrypt(idx_seq)
            decrypted_text = [alpha_list[j] for j in decrypted_idx_seq]
            return(self.list_to_string(decrypted_text))
        else:
            return 'Only Text Values'