import numpy as np
import string

class PermutationCipher:
    def __init__(self) -> None:
        pass

    def encrypt_message_using_keyword(self, plaintext, encrypt_keyword):
        self.plaintext = plaintext
        self.encrypt_keyword = encrypt_keyword

        encryptedMessage = None
        alpha_list = []
        for alpha in string.ascii_lowercase:
            alpha_list.append(alpha)
        
        self.plaintext_clean = (self.plaintext.lower()).replace(' ','')
        
        if self.plaintext_clean.isalpha():
            idx_seq = []
            idx_seq_enc_key = []
            for ltr in self.plaintext_clean:
                idx_in_alpha_list = alpha_list.index((ltr).lower())
                idx_seq.append(idx_in_alpha_list)
            for ltr in self.encrypt_keyword:
                idx_in_alpha_list = alpha_list.index((ltr).lower())
                idx_seq_enc_key.append(idx_in_alpha_list)
        else:
            return 'Only text values'

        coll_data = idx_seq_enc_key + idx_seq
        
        # Converting to NpyArr
        conv_nparr_idx_seq = np.array(coll_data)
        
        # Dynamic selection of m-dimension using modAri
        # modulus = len(self.encrypt_keyword)
        # size = len(coll_data)
        # r = size % modulus
        # diff = modulus - r
        # print(diff)
        # m_dim = size + diff
        # m = int(m_dim / modulus)
        # n = modulus
        m = 13
        n = 3

        cis = conv_nparr_idx_seq.reshape(m,n)
        sort_arr = (cis[:, cis[0].argsort()]).tolist()
        keyword_key = sort_arr.pop(0)
        encryptedMessage =''
        for x in sort_arr:
            encrypted_string = ''
            for j in x:
                encrypted_string = encrypted_string + (alpha_list[j])
            encryptedMessage = encryptedMessage + (encrypted_string) + ' '

        return encryptedMessage

    def decrypt_message_using_keyword(self, ciphertext, decryp_keyword):
        self.ciphertext = ciphertext
        self.decryp_keyword = decryp_keyword

        decryptedMesage = ''
        alpha_list = []
        for alpha in string.ascii_lowercase:
            alpha_list.append(alpha)
        
        # Analyzing Keyword
        len_dkey = len(self.decryp_keyword)
        seq_dkey= []
        for ltr in self.encrypt_keyword:
            idx_in_alpha_list = alpha_list.index((ltr).lower())
            seq_dkey.append(idx_in_alpha_list)
        seq_dkey_outorder = seq_dkey
        seq_dkey_inorder = sorted(seq_dkey)
        dkey_inorder = ''
        for j in seq_dkey_inorder:
            dkey_inorder = dkey_inorder + (alpha_list[j])
        
        # Processing cipherText
        cipher_arr = []
        tmp = []
        cnct_text = dkey_inorder + ' ' + self.ciphertext
        for i in cnct_text:
            if i != ' ':
                idx = alpha_list.index(i)
                tmp.append(idx)
            else:
                cipher_arr.append(tmp)
                tmp = []
        
        # # convert python list to ndarry
        cna = np.array(cipher_arr)
        sort_arr = (cna[:, (np.array(seq_dkey_outorder)).argsort()]).tolist()
        sort_arr.pop(0)

        # Converting arr tot text
        for x in sort_arr:
            for y in x:
                decryptedMesage = decryptedMesage + alpha_list[y]
        
        return decryptedMesage

    def decrypt_message_using_number(self, ct, ek):
        self.ek = str(ek)
        ek_list = [int(i) for i in self.ek]
        dk_list = [ek_list.index(j + 1) + 1 for j in range(0, len(ek_list))]
        len_ek_list = len(ek_list)
        len_ct= len(ct)

        plaintext = ''
        tmp = []
        for k,v in enumerate(ct):
            len_tmp = len(tmp)
            if len_tmp < len_ek_list and len_ct != k + 1:
                tmp.append(v)
            elif len_tmp == len_ek_list:
                for key,value in enumerate(tmp):
                    dk_key = dk_list[key]
                    dk_value = tmp[dk_key - 1]
                    plaintext = plaintext + dk_value
                tmp = []
                tmp.append(v)
            else:
                tmp.append(v)
                for key,value in enumerate(tmp):
                    dk_key = dk_list[key]
                    dk_value = tmp[dk_key - 1]
                    plaintext = plaintext + dk_value
                tmp = []
                tmp.append(v)
        return plaintext