import string
import math

class AffineCipher():
    def __init__(self) -> None:
        pass
    
    def encrypt(self, idx_seq):
        encrypted_idx_seq = []
        for idxValue in idx_seq:
            encVal = (self.alpha * idxValue + self.beta) % self.modulus
            encrypted_idx_seq.append(encVal)
        return encrypted_idx_seq

    def decrypt(self, idx_seq):
        decrypted_idx_seq = []
        for idxValue in idx_seq:
            multiInvAlpha = pow(self.alpha, -1, self.modulus)
            deVal = (multiInvAlpha * (idxValue - self.beta)) % self.modulus
            decrypted_idx_seq.append(deVal)
        return decrypted_idx_seq

    def list_to_string(self,list):
        listToStr = ''.join(map(str,list))
        return listToStr
    
    def encryptMessage(self, message, alpha, beta):
        self.message = message
        self.alpha = alpha
        self.beta = beta
        encrypted_text= None
        en_alphabet_list = []
        for alpha in string.ascii_lowercase:
            en_alphabet_list.append(alpha)
        self.modulus = len(en_alphabet_list)

        # Condition 1: GCD of alpha & 26 ( since EN lang) whould be 1
        divisor = math.gcd(self.alpha, self.modulus)
        if divisor == 1:
            if (1 <= self.alpha <= self.modulus):
                if (1 <= self.beta <= self.modulus):
                    if self.message.isalpha():
                        idx_seq = []
                        for ltr in self.message:
                            idx_in_alpha_list = en_alphabet_list.index((ltr).lower())
                            idx_seq.append(idx_in_alpha_list)
                        encrypted_idx_seq = self.encrypt(idx_seq)
                        encrypted_text = [en_alphabet_list[j] for j in encrypted_idx_seq]
                        return(self.list_to_string(encrypted_text))
                    else:
                        return 'Only text values'
                else:
                    return 'Cond 2: of Affine Cipher not met.'
            else:
                return 'Cond 2: of Affine Cipher not met.'
        else:
            return 'Cond 1: of Affine Cipher not met.'
    
    def decryptMessage(self, message, alpha, beta):
        self.message = message
        self.alpha = alpha
        self.beta = beta
        decrypted_text= None
        en_alphabet_list = []
        for alpha in string.ascii_lowercase:
            en_alphabet_list.append(alpha)
        self.modulus = len(en_alphabet_list)

        # Condition 1: GCD of alpha & 26 ( since EN lang) whould be 1
        divisor = math.gcd(self.alpha, self.modulus)
        if divisor == 1:
            if (1 <= self.alpha <= self.modulus):
                if (1 <= self.beta <= self.modulus):
                    if self.message.isalpha():
                        idx_seq = []
                        for ltr in self.message:
                            idx_in_alpha_list = en_alphabet_list.index((ltr).lower())
                            idx_seq.append(idx_in_alpha_list)
                        decrypted_idx_seq = self.decrypt(idx_seq)
                        decrypted_text = [en_alphabet_list[j] for j in decrypted_idx_seq]
                        return(self.list_to_string(decrypted_text))
                    else:
                        return 'Only text values'
                else:
                    return 'Cond 2: of Affine Cipher not met.'
            else:
                return 'Cond 2: of Affine Cipher not met.'
        else:
            return 'Cond 1: of Affine Cipher not met.'
    
    def num_affine_keys(self,num):
        def is_coprime(x, y):
            return math.gcd(x, y) == 1
        def phi_func(x):
            if x == 1:
                return 1
            else:
                n = [y for y in range(1,x) if is_coprime(x,y)]
                return len(n)
        return (num * phi_func(num))

    def num_involutory_affine_keys(self,num):
        def is_coprime(x, y):
            return math.gcd(x, y) == 1
        def phi_func(x):
            if x == 1:
                return 1
            else:
                n = [y for y in range(1,x) if is_coprime(x,y)]
                return len(n)
        return (num * phi_func(num))