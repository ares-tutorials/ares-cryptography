import string
import math

class VigenereCipher():
    def __init__(self) -> None:
        pass
    
    def list_to_string(self,list):
        listToStr = ''.join(map(str,list))
        return listToStr
    
    def encryptMessage(self, message, keyword):
        self.message = message
        self.keyword = keyword
        encrypted_text= None
        
        # EN alpha list
        en_alphabet_list = []
        for alpha in string.ascii_lowercase:
            en_alphabet_list.append(alpha)
        self.modulus = len(en_alphabet_list)

        # Key-Keyword idx_list
        if self.keyword.isalpha():
            idx_seq_kwd = []
            for ltr in self.keyword:
                idx_in_alpha_list = en_alphabet_list.index((ltr).lower())
                idx_seq_kwd.append(idx_in_alpha_list)
        else:
            return 'Only text values for keyword-key'
        kwdListLen = len(idx_seq_kwd)
        # Condition 1: GCD of alpha & 26 ( since EN lang) whould be 1
        if self.message.isalpha():
            idx_seq_msg = []
            for ltr in self.message:
                idx_in_alpha_list = en_alphabet_list.index((ltr).lower())
                idx_seq_msg.append(idx_in_alpha_list)
            
            enc_idx_list = []
            lastIdxKwd = None
            for i in idx_seq_msg:
                tmp = []
                if lastIdxKwd is not None and lastIdxKwd + 1 == (kwdListLen):
                    lastIdxKwd = 0
                elif lastIdxKwd is None:
                    lastIdxKwd = 0
                else:
                    lastIdxKwd = lastIdxKwd + 1
                tmp.append(i)
                tmp.append(idx_seq_kwd[lastIdxKwd])

                residue = (tmp[0] + tmp[1]) % self.modulus
                enc_idx_list.append(residue)
            encrypted_text = [en_alphabet_list[j] for j in enc_idx_list]
            return(self.list_to_string(encrypted_text))
        else:
            return 'Only text values'